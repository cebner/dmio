/*
 * Copyright 2018 Christian Ebner
 *
 * This file is part of dmio.
 *
 * dmio is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * dmio is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with dmio.  If not, see <http://www.gnu.org/licenses/>.
 */

// Reader and writer version types
pub enum DM3Writer {}
pub enum DM4Writer {}
pub enum DM3Reader {}
pub enum DM4Reader {}

// Version constants
pub const DM3: usize = 3usize;
pub const DM4: usize = 4usize;

// Tag type constants
pub const T_TAG: u8 = 21u8;
pub const T_GROUP: u8 = 20u8;
pub const T_SIMPLE: usize = 1usize;
pub const T_STRING: usize = 2usize;
pub const T_ARRAY: usize = 3usize;

// Numeric type constants
pub const D_ZERO: usize = 0usize;
pub const D_SHORT: usize = 2usize;
pub const D_LONG: usize = 3usize;
pub const D_USHORT: usize = 4usize;
pub const D_ULONG: usize = 5usize;
pub const D_FLOAT: usize = 6usize;
pub const D_DOUBLE: usize = 7usize;
pub const D_BOOLEAN: usize = 8usize;
pub const D_CHAR: usize = 9usize;
pub const D_OCTET: usize = 10usize;
pub const D_ULONGLONG: usize = 11usize;
pub const D_UNKNOWN64: usize = 12usize; // TODO figure out type
pub const D_STRUCT: usize = 15usize;
pub const D_STRING: usize = 18usize;
pub const D_ARRAY: usize = 20usize;

pub const BIG_ENDIAN: usize = 0usize;
pub const LITTLE_ENDIAN: usize = 1usize;

// Boolean type consts
pub const B_TRUE: u8 = 1u8;
pub const B_FALSE: u8 = 0u8;

pub const SORTED: bool = true;
pub const OPEN: bool = true;
