/*
 * Copyright 2018 Christian Ebner
 *
 * This file is part of dmio.
 *
 * dmio is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * dmio is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with dmio.  If not, see <http://www.gnu.org/licenses/>.
 */

use super::{DMImage, DM3Writer, DM4Writer};

#[test]
fn test_dmimage_new() {
    let _image = DMImage::new(&[10]);
}

#[test]
fn test_dmimage_to_raw_rgb() {
    let image = DMImage::open("testfiles/dm3_image.dm3").unwrap();
    let _rgb = image.to_raw_rgb();
}

#[test]
fn test_dm3image_open_and_save_as() {
    let mut image = DMImage::open("testfiles/dm3_image.dm3").unwrap();
    let _ = image.save_as::<DM3Writer>("testfiles/test_dm3image_save_as.dm3").unwrap();
}

#[test]
fn test_dm4image_open_and_save_as() {
    let mut image = DMImage::open("testfiles/dm4_image.dm4").unwrap();
    let _ = image.save_as::<DM4Writer>("testfiles/test_dm4image_save_as.dm4").unwrap();
}
