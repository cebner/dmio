About dmio
==========

dmio is free software!
It is a shared library allowing you to read and write the proprietary DigitalMicrograph<sup>TM</sup> version 3 and version 4 (dm3, dm4) file format.
This file format is commonly used for Transmission Electron Microscopy (TEM) images and other experimental data such as line spectra.
dmio is written in Rust based upon previous knowledge about the file format found on the internet.

Status
======

At the moment the library is in an early development stage and functionality is limited.
The main functionality of parsing and writing the files is already implemented and basic testing was performed.
Please notice that this code is a private hobby project with the main purpose of getting better at programming Rust.
For the future functionality extensions such as e.g. a FFI for C and Python are planed.

License
=======

This software is licensed under the LGPL-3.0-or-later.

You should have received a copy of the GNU Lesser General Public License
along with dmio.  If not, see <http://www.gnu.org/licenses/>.
